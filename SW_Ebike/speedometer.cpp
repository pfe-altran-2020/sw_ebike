#include "speedometer.h"
#include <def_types.h>


speedometer::speedometer(QObject *parent) : QObject(parent),m_speed(0),m_timer(0) {
    timer = new QTimer(this);
    timer->setInterval(100);
    connect(timer,&QTimer::timeout,this,&speedometer::s_increaseSpeed);
    timer->start(500);
}

speedometer::~speedometer() {
    delete timer;
}

/** This function increase speed from 0 to 60 in Km/h
 *
 * @param[in,out,global private]     m_timer    time in second  {values: 200.0f .. 400.0f}
 * @param[in,out,global private]     m_speed    speed in Km/h   {values: .0f .. 60.0f}
 * @return  void
 */
void speedometer::s_increaseSpeed() {
#ifdef MODE_TEST_ACTIVE

    if (m_timer < 400) {

        if (m_timer > 200) {

            if (m_speed < 60) {

                m_speed += 1;

                emit s_valueChanged();

            } else {
                /* Nothing TO DO */
            }

            m_timer += 1;

        } else {
            /* Nothing TO DO */
        }

        m_timer += 1;

    } else {
        /* Nothing TO DO */
    }
#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else

#endif
}

/** This function return speed
 *
 * @param[out,global private]  m_speed   speed in km/h         {values: .0f .. 60.f}
 * @return  float
 */
qreal speedometer::s_getSpeed() {
    return m_speed;
}


