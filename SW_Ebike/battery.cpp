#include "battery.h"
#include <QTimer>
#include <QEventLoop>
#include <def_types.h>

battery::battery(QObject *parent) : QObject(parent),m_charge(0.f),m_segOneCounter(0),m_segOne(0.f),m_segTwoCounter(0),m_segTwo(0.f),m_segThreeCounter(0),m_segThree(0.f),m_segFourCounter(0),m_segFour(0.f),m_segFiveCounter(0),m_segFive(0.f),m_decharge(0.f),m_timeDecharge(0),m_time(0) {
    timer = new QTimer(this);
    timer->setInterval(B_TIME_INTERVAL_MS);
    connect(timer,&QTimer::timeout,this,&battery::b_State_of_charge);
    connect(timer,&QTimer::timeout,this,&battery::b_chargeSegOne);
    connect(timer,&QTimer::timeout,this,&battery::b_chargeSegTwo);
    connect(timer,&QTimer::timeout,this,&battery::b_chargeSegThree);
    connect(timer,&QTimer::timeout,this,&battery::b_chargeSegFour);
    connect(timer,&QTimer::timeout,this,&battery::b_chargeSegFive);
    connect(timer,&QTimer::timeout,this,&battery::b_batteryDecharge);
    timer->start(B_TIME_START_MS);
}


battery::~battery() {
    delete timer;
}

/** This function display battery percentge
 *
 * @param[in,out,global private]  m_time      time in second    {values: 0 .. 400}
 * @param[out,global private]     m_charge    charge percentge  {values: .0f .. 100.f}
 * @return  void
 */
void battery::b_State_of_charge() {
#ifdef MODE_TEST_ACTIVE
    if (m_time < 400) {

        m_charge += 0.5f;

        emit b_valueChanged();

        m_time += 1;

        if (m_time > 200) {

            m_charge -= 1.f;

        } else {
            /* Nothing TO DO */
        }

        emit b_valueChanged();

    } else {
        /* Nothing TO DO */
    }
#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else

#endif
}

/** This function return charge percentage
 *
 * @param[out,global private]     m_charge    charge percentge  {values: .0f .. 100.f}
 * @return  float
 */
qreal battery :: b_getCharge() {
    return m_charge;
}


/** This function display the first segment of the battery
 *
 * @param[in,out,global private]     m_segOne          length of the first segment    {values: 0 .. 25}
 * @param[in,out,global private]     m_segOneCounter   counter of the first segment   {values: 0 .. 39}
 * @return  void
 */
void battery::b_chargeSegOne() {
#ifdef MODE_TEST_ACTIVE

    if (m_segOneCounter < 39) {

        m_segOne += 25.f;

        if (m_segOne > 25.f) {

            m_segOne -= 50.f;

        } else {
            /* Nothing TO DO */
        }

        emit b_valueChanged();

        m_segOneCounter += 1;

    } else {
        /* Nothing TO DO */
    }

#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else
#endif
}


/** this function loads the first segment
 *
 * @param[out,global private]     m_segOne     length of the first segment   {values: 0 .. 25}
 * @return  float
 */
qreal battery::b_getSegOneCharged()
{
    return m_segOne;
}


/** This function display the second segment of the battery
 *
 * @param[in,out,global private]     m_segTwo          length of the second segment  {values: 0 .. 25}
 * @param[in,out,global private]     m_segTwoCounter   counter of the second segment {values: 39 .. 117}
 * @return  void
 */
void battery :: b_chargeSegTwo() {
#ifdef MODE_TEST_ACTIVE
    if (m_segTwoCounter < 117) {

        if (m_segTwoCounter > 39) {

            m_segTwo += 25.f;

            if (m_segTwo > 25.f) {

                m_segTwo -= 50.f;

            } else {
                /* Nothing TO DO */
            }

            m_segTwoCounter += 1;

            emit b_valueChanged();

        } else {
            /* Nothing TO DO */
        }

        m_segTwoCounter += 1;

    } else {
        /* Nothing TO DO */
    }

#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else
#endif
}



/** this function loads the second segment
 *
 * @param[out,global private]     m_segTwo          length of the second segment  {values: 0 .. 25}
 * @return  float
 */

qreal battery::b_getSegTwoCharged()
{
    return m_segTwo;
}


/** This function display the third segment of the battery
 *
 * @param[in,out,global private]     m_segThree          length of the third segment   {values: 0 .. 25}
 * @param[in,out,global private]     m_segThreeCounter   counter of the third segment  {values: 78 .. 156}
 * @return  void
 */
void battery :: b_chargeSegThree() {
#ifdef MODE_TEST_ACTIVE

    if (m_segThreeCounter < 156) {

        if (m_segThreeCounter > 78) {

             m_segThree += 25.f;

             if (m_segThree > 25.f) {

                 m_segThree -= 50.f;

             } else {
                 /* Nothing TO DO */
             }

             m_segThreeCounter += 1;

             emit b_valueChanged();

        } else {
            /* Nothing TO DO */
        }

        m_segThreeCounter += 1;

    } else {
        /* Nothing TO DO */
    }

#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else
#endif
}


/** this function loads the third segment
 *
 * @param[out,global private]     m_segThree          length of the third segment  {values: 0 .. 25}
 * @return  float
 */
qreal battery::b_getSegThreeCharged()
{
    return m_segThree;
}


/** This function display the fourth segment of the battery
 *
 * @param[in,out,global private]     m_segFour         length of the fourth segment   {values: 0 .. 25}
 * @param[in,out,global private]     m_segFourCounter  counter of the fourth segment  {values: 117 .. 195}
 * @return  void
 */
void battery :: b_chargeSegFour() {

#ifdef MODE_TEST_ACTIVE

    if (m_segFourCounter < 195) {

       if (m_segFourCounter > 117) {

           m_segFour += 25.f;

           if (m_segFour > 25.f) {

               m_segFour -= 50.f;

           } else {
               /* Nothing TO DO */
           }

           m_segFourCounter += 1;

           emit b_valueChanged();

       } else {
           /* Nothing TO DO */
       }

        m_segFourCounter += 1;

    } else {
        /* Nothing TO DO */
    }
}
#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else
#endif
/** this function loads the fourth segment
*
* @param[out,global private]     m_segFour          length of the fourth segment  {values: 0 .. 25}
* @return  float
*/
qreal battery::b_getSegFourCharged()
{
    return m_segFour;
}


/** This function display the fifth segment of the battery
 *
 * @param[in,out,global private]     m_segFive         length of the fifth segment    {values: 0 .. 25}
 * @param[in,out,global private]     m_segFiveCounter  counter of the fifth segment   {values: 117 .. 195}
 * @return  void
 */
void battery :: b_chargeSegFive() {
#ifdef MODE_TEST_ACTIVE
    if (m_segFiveCounter < 238) {

        if (m_segFiveCounter > 156) {

            m_segFive += 25.f;

            if (m_segFive > 25.f) {

                m_segFive -= 50.f;

            } else {
                /* Nothing TO DO */
            }

            m_segFiveCounter += 1;

            emit b_valueChanged();

        } else {
            /* Nothing TO DO */
        }

    m_segFiveCounter += 1;

    } else {
        /* Nothing TO DO */
    }
}
#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else
#endif


/** this function loads the fifth segment
*
* @param[out,global private]     m_segFive          length of the fifth segment   {values: 0 .. 25}
* @return  float
*/
qreal battery::b_getSegFiveCharged()
{
    return m_segFive;
}


/** This function discharge the battery
 *
 * @param[in,out,global private]     m_decharge        discharge battery value    {values: .0f .. 136.f}
 * @param[in,out,global private]     m_timeDecharge    time of discharge battery  {values: 200 .. 7000}
 * @return  void
 */
void battery::b_batteryDecharge() {
#ifdef MODE_TEST_ACTIVE

    if (m_timeDecharge < 7000){

       if (m_timeDecharge > 200) {

           if (m_decharge < 136.f) {

               m_decharge += 0.68;

               emit b_valueChanged();

           } else {
               /* Nothing TO DO */
           }

           m_timeDecharge += 1;
       } else {
           /* Nothing TO DO */
       }

       m_timeDecharge += 1;

    } else {
        /* Nothing TO DO */
    }

#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else
#endif
}

 /** This function return
 *
 * @param[out,global private]     m_decharge         length of the fiifth segment  {values: .0f .. 136.f}
 * @return  float
 */
qreal battery::b_getBatteryDecharge()
{
    return m_decharge;
}
