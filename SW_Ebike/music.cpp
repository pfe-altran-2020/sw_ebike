#include "music.h"
#include <QQuickItem>
#include <QQmlProperty>
#include <QQmlEngine>

music::music(QObject *parent) : QObject(parent), m_progressBar(0), m_progressTime(0)
{
    m_player =  new QMediaPlayer(this); // Dynamic instantiation
    m_player->setMedia(QUrl::fromLocalFile("/Users/Dell/Desktop/SW_Ebike/Test.mp3"));
    m_player->volume();
    timer = new QTimer(this);
    timer->setInterval(100);
}

music::~music()
{
    delete timer;
    delete m_player;
}

/** This function display progress bar
 * @param[in,out,global private]     m_progress    music progress bar   {values: 0 .. 360}
 * @return  void
 */
void music::m_play() {
    if(m_progressBar < 348) {
        m_progressBar+=12;
    } else {

        /* Nothing to do here - flies away
         *             .=.,
         *            ;c =\
         *          __|  _/
         *        .'-'-._/-'-._
         *       /..   ____    \
         *      /' _  [<_->] )  \
         *     (  / \--\_>/-/'._ )
         *      \- _/\__ __/ _/ _/
         *       '._}|==o==\{_\/
         *        /  /-._.--\  \_
         *       /  /   /|   \ \ \
         *      / | |   | \;  |  \ \
         *     / /  | :/   \: \   \_\
         *    /  |  /.'|   /: |    \ \
         *    |  |  |--| . |--|     \_\
         *    / _/   \ | : | /___--._) \
         *   |_(---'-| >-'-| |       '-'
         *          /_/     \_\
         */
        m_progressBar = 0;
        disconnect(timer,&QTimer::timeout,this,&music ::m_play);
    }
    emit m_valueChanged();
}

/** This function controls musicplayer button
 *
 * @param[in,out,global private]     timer          timer for musicplayer
 * @param[in,out,global private]     m_player       a new qmediaplayer
 * @param[in,out,global private]     stateButton    boolean
 * @return  void
 */
void music::onClickedstartstop(void)
{
    static bool stateButton = false;

    if (stateButton == false) {
        m_player->play();
        timer->start(1000); // in ms
        stateButton = true;
        connect(timer,&QTimer::timeout,this,&music ::m_play);
    } else {
        m_player->pause();
        timer ->stop();
        stateButton = false;
        disconnect(timer,&QTimer::timeout,this,&music ::m_play);
    }
}

/** This function return the progressbar value
 *
 * @param[out,global private]  m_progress      progressbar value   {values: 0 .. 360}
 * @return  int
 */
qint32 music::m_getprogressBar() {
    return m_progressBar;
 }

/** This function return the progress time
 *
 * @param[out,global private]  m_progress      progress time value   {values: 0 .. 360}
 * @return  int
 */
qint64 music::m_getprogressTime() {
    return m_progressTime;
 }

