import QtQuick 2.4
import QtGraphicalEffects 1.0

Rectangle {
    color: "transparent"
    transformOrigin: Item.Center
    rotation: 0
    z: 2

    SpeedNeedle {
                  id: speedoNeedle
                  width: 507
                  height: 480
                  angleOffset: 27
                  anchors.horizontalCenterOffset: -120
                  secondaryColor: "#34faf8"
                  maximumValue: 0
                  rotation: 0

                       anchors.verticalCenterOffset: 0
                       anchors.centerIn: parent

                       focus: true
                     value:acceleration.getspeed;
}

              InnerRing    {
                id: innerring
                x: 31
                y: 157
                width: 210
                height: 214
                z: 3
                rotation: 0
                transformOrigin: Item.Bottom
                speed: acceleration.getspeed;
              }



 }

