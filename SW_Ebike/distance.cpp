#include "distance.h"
#include <def_types.h>


distance::distance(QObject *parent) : QObject(parent),m_distanceCounter(0), m_distance(0.0f) {
    timer = new QTimer(this);
    timer->setInterval(D_TIME_INTERVAL_MS);
    connect(timer,&QTimer::timeout,this,&distance::d_count);
    timer->start(D_TIME_START_MS);
}

distance::~distance() {
    delete timer;
}

/** This function return distance traveled by E_bike
 *
 * @param[in,out,global private]  m_distanceCounter      time             {values: 200 .. 800}
 * @param[in,out,global private]  m_distance             distance in km   {values: .0f .. 99.0f}
 * @return  void
 */
void distance::d_count() {
#ifdef MODE_TEST_ACTIVE
    if (m_distanceCounter < 800){

        if (m_distanceCounter > 200) {

            if (m_distance < 99) {

                m_distance += 0.5f;

                emit d_valueChanged();

            } else {
                /* Nothing TO DO */
            }

            m_distanceCounter += 1;

        } else {
            /* Nothing TO DO */
        }

        m_distanceCounter += 1;

    } else {
        /* Nothing TO DO */
    }

#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else

#endif
}

/** This function return distance
 *
 * @param[out,global private]  m_distance   distance in km         {values: .0f .. 99.0f}
 * @return  float
 */
qreal distance::d_getDistance()
{
    return m_distance;
}
