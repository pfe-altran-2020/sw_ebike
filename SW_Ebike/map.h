#ifndef MAP_H
#define MAP_H

#include <QObject>

class Map : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qint32 coordinate READ coordinate  WRITE setcoordinate NOTIFY valueChanged)

signals:
    void valueChanged(qint32 coordinate);

public:
    Map();
    virtual ~Map();
    qint32 coordinate();
    void setcoordinate(qint32 coordinate);

public slots:
    void receivecoordinate( qint32 coordinate);

private:
    qint32 m_coordinate;

};

#endif // M_H
