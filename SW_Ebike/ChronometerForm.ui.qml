import QtQuick 2.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.3

Item {
    width: 480
    height: 260

    Rectangle {
        id: rectangle
        color: "#323232"
        anchors.fill: parent
    }
}

/*##^##
Designer {
    D{i:1;anchors_height:200;anchors_width:200;anchors_x:150;anchors_y:21}
}
##^##*/
