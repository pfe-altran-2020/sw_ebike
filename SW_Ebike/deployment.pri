unix:!android {
    isEmpty(target.path) {
        qnx {
            target.path = /tmp/$${TARGET}/bin
        } else {
            target.path = /home/pi/projects   # Path for deploying application on raspberry pi 2
        }
        export(target.path)
    }
    INSTALLS += target
}

export(INSTALLS)
