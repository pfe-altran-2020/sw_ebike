import QtQuick 2.4
import QtQuick.Extras 1.4

Item {
    width: 450
    height: 260
    Rectangle {
        id: leGrand
        width: 450
        height: 260
        color: "#323232"
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
    }
}

/*##^##
Designer {
    D{i:2;anchors_x:8}
}
##^##*/

