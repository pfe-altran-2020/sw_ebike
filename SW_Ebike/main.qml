import QtQuick 2.12
//import QtQuick.Window 2.12
import QtQuick.Controls 2.3
import QtQuick.Extras 1.4
import QtQuick.Controls.Material 2.0
import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.0

ApplicationWindow {
    id: window
    visible: true
    width: 480
    height: 260

    Intro{

    }

    Timer{
        id: closeTimer
        interval: 5000
        repeat: false
        running: true

        onTriggered: {

            closeTimer.running = false
            swipeView.visible = true


        }
    }

    StackLayout {
        id: swipeView
        x: 0
        y: -1
        width: 480
        height: 260
        visible: false

        currentIndex: tabBar.currentIndex

        Speed {
            id: speed
            x: 0
            width: 480
        }

        BikeMode  {
            id: settings
            x: 0
            y: 0
            width: 480
        }

       Music {
            id: music
            x: 0
            width: 480
        }


        Chronometer{
            id: chrono
            x: 0
            width: 480
            height: 260
        }

    Map {
        id: map
        x: 0
        width: 480
        height: 260
    }

    Youtube {
            id: youtube
            x: 0
            width: 480
            height: 260
        }

        Google {
            id: google
            x: 0
            width: 480
            height: 260
        }

    


    Drawer {
            id: drawer
            x: 0
            y: 0
            width: 60
            height: 300

        ScrollView {
            id: scrollView
            x: -90
            y: 100
            width: 260
            height: 60
            rotation: 90


        TabBar{
            id:tabBar
            x: 0
            y: 18
            width: 420
            height: 60
       //     rotation: 90

            currentIndex: swipeView.currentIndex
            TabButton {
                id: tabButton1
                x: 0
                y: 0
                width: 60
                height: 60
                text: qsTr("")


                Image {
                    id: image1
                    x: 10
                    y: 10
                    width: 40
                    height: 40
                    rotation: -90
                    source: "pics/11.png"
                    fillMode: Image.PreserveAspectFit
                }
            }

            TabButton{
                id: tabButton2
                x: 60
                y: 0
                width: 60
                height: 60
                text: qsTr("")

                Image {
                    id: image2
                    y: 10
                    width: 40
                    height: 40
                    anchors.verticalCenterOffset: 0
                    anchors.horizontalCenterOffset: 0
                    rotation: -90
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                    fillMode: Image.PreserveAspectFit
                    source: "pics/10.png"
                }
                

            }
            TabButton {
                id: tabButton3
                x: 120
                y: 0
                width: 60
                height: 60
                text: qsTr("")

                Image {
                    id: image3
                    x: 10
                    y: 10
                    width: 40
                    height: 40
                    rotation: -90
                    source: "pics/music.png"
                    fillMode: Image.PreserveAspectFit
                }
            }

            TabButton {
                id: tabButton4
                x: 180
                y: 0
                width: 60
                height: 60
                visible: true

                Image {
                    id: image4
                    x: 10
                    y: 10
                    width: 40
                    height: 40
                    rotation: -90
                    fillMode: Image.PreserveAspectFit
                    source: "pics/times.png"
                }
            }

            TabButton {
                id: tabButton5
                x: 240
                y: 0
                width: 60
                height: 60
                text: qsTr("")

                Image {
                    id: image5
                    x: 10
                    y: 10
                    width: 40
                    height: 40
                    rotation: -90
                    source: "pics/8.png"
                    fillMode: Image.PreserveAspectFit
                }
            }
            TabButton {
                      id: tabButton6
                      x: 240
                      y: 0
                      width: 60
                      height: 60
                      text: qsTr("")

                      Image {
                          id: image6
                          x: 10
                          y: 10
                          width: 40
                          height: 40
                          rotation: -90
                          source: "pics/Youtube1.png"
                          fillMode: Image.PreserveAspectFit
                      }
                  }
                  TabButton {
                      id: tabButton7
                      x: 240
                      y: 0
                      width: 60
                      height: 60
                      text: qsTr("")

                      Image {
                          id: imagesearch
                          x: 10
                          y: 10
                          width: 40
                          height: 40
                          rotation: -90
                          source: "pics/4.png"
                          fillMode: Image.PreserveAspectFit
                      }
                  }



}
    }
}
}
}
