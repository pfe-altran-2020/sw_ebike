#ifndef DISTANCE_H
#define DISTANCE_H
#include <QObject>
#include <QTimer>
#include <QDebug>


#define D_TIME_INTERVAL_MS  (100)
#define D_TIME_START_MS     (500)


class distance : public QObject
{
     Q_OBJECT
public:
    Q_PROPERTY(quint32 d_getdistance READ d_getDistance NOTIFY d_valueChanged)
    explicit distance(QObject *parent = nullptr);
    virtual ~distance();
    qreal d_getDistance();

public slots :
     void d_count(void);

signals :
     void d_valueChanged();

private :
    qint32 m_distanceCounter;
    qreal  m_distance;
    QTimer *timer;
};

#endif // DISTANCE_H
