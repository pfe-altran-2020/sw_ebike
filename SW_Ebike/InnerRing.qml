import QtQuick 2.0
import QtGraphicalEffects 1.0
Item {

    property int speed: 0



         Text {
             id: speeddigit
             text: speed
             scale: 1.2
             anchors.horizontalCenterOffset: -19
             font.pixelSize: 86
             font.bold: true
             font.family: "Arial"
             y: 49
             width: 46
             height: 93
             color: "white"
             anchors.horizontalCenter: parent.horizontalCenter
         }

         Text {
             text: "km/h"
             font.underline: false
             font.italic: false
             anchors.horizontalCenterOffset: 114
             font.pixelSize: 24
             font.bold: false
             font.family: "Eurostile"
             y: 103
             color: "#948989"
             anchors.horizontalCenter: parent.horizontalCenter
         }


    }

