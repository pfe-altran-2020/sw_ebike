#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <speedometer.h>
#include <battery.h>
#include <turn_signal_light.h>
#include <distance.h>
#include "clockcircle.h"
#include <music.h>
#include <QQmlProperty>
#include <QQmlComponent>
#include <QQmlContext>
#include "map.h"

int main(int argc, char *argv[])
{

    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QGuiApplication app(argc, argv);
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    music  squareOne;
    speedometer acceleration;
    battery batteri;
    battery partOne;
    turn_signal_light arrow;
    battery partTwo;
    battery partThree;
    battery partFour;
    battery partFive;
    battery bat;
    distance counter;
    Map hMap;

    qmlRegisterType<ClockCircle>("ClockCircle",1,0,"ClockCircle");

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("squareOne", &squareOne);
    engine.rootContext()->setContextProperty("acceleration", &acceleration);
    engine.rootContext()->setContextProperty("batteri", &batteri);
    engine.rootContext()->setContextProperty("arrow", &arrow);
    engine.rootContext()->setContextProperty("partOne", &partOne);
    engine.rootContext()->setContextProperty("partTwo", &partTwo);
    engine.rootContext()->setContextProperty("partThree", &partThree);
    engine.rootContext()->setContextProperty("partFour", &partFour);
    engine.rootContext()->setContextProperty("partFive", &partFive);
    engine.rootContext()->setContextProperty("bat", &bat);
    engine.rootContext()->setContextProperty("counter", &counter);
    engine.rootContext()->setContextProperty("hMap", &hMap);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
