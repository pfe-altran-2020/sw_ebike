import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.12
import QtWebEngine 1.8
import QtLocation 5.12
import QtPositioning 5.12
import QtQuick.VirtualKeyboard 2.14

MapForm {

    InputPanel {
        id: inputPanel
        z: 99
        x: 0
        y: window.height
        width: window.width

        states: State {
            name: "visible"
            when: inputPanel.active
            PropertyChanges {
                target: inputPanel
                y: window.height - inputPanel.height
            }
        }
        transitions: Transition {
            from: ""
            to: "visible"
            reversible: true
            ParallelAnimation {
                NumberAnimation {
                    properties: "y"
                    duration: 250
                    easing.type: Easing.InOutQuad
                }
            }
        }
    }

    SwipeView {
        id: view

        currentIndex: 0
        anchors.fill: parent

        Item {
            id: vectorTab

            Plugin {
                id: mapPlugin
                name: "osm"
            }

            property var initialPosition: QtPositioning.coordinate(51.508530, -0.076132)
            Map {
                anchors.topMargin: 1
                anchors.fill: parent
                plugin: mapPlugin
                center: initialPosition
                zoomLevel: 20
                objectName: "osmMap"

                MapCircle {
                    objectName: "mapCircleStart"
                    center: initialPosition
                    color: "red"
                    opacity: 0.7
                    radius: 2.5
                }
            }
            PositionSource {
                id: src
                updateInterval: 1000
                active: true

                onPositionChanged: {
                    var coord = hMap.coordinate;
                    console.log("Coordinate:", coord.longitude, coord.latitude);
                }
            }

        }



        Item {
            id: webBrowserTab

            WebEngineView {
                anchors.fill: parent
                     url: "https://www.google.com/maps/@36.4649994,8.9438143,8z?hl=fr"
                }

        }






        }






    Rectangle {
            id: rectangle4
            width: 80
            height: 80
            color: "#000000"
            radius: 40
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5

            Rectangle {
                id: rectangle5
                x: 0
                y: 54
                width: 40
                height: 40
                color: "#000000"
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0

                Rectangle {
                    id: rectangle11
                    y: 23
                    width: 2
                    height: 10
                    color: "#323232"
                    anchors.left: parent.left
                    anchors.leftMargin: 5
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 5
                }

                Rectangle {
                    id: rectangle12
                    y: 38
                    width: 2
                    height: 10
                    color: "#323232"
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    rotation: 90
                }
            }

            Text {
                id: element1
                x: -1
                y: 39
                width: 19
                height: 22
                color: "#ffffff"
                text: acceleration.getspeed
                z: 7
                font.pixelSize: 25
                anchors.horizontalCenterOffset: -1
                anchors.verticalCenterOffset: -6
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
            }

            Text {
                id: element3
                y: 53
                color: "#323232"
                text: qsTr("km/h")
                anchors.horizontalCenterOffset: 0
                anchors.verticalCenterOffset: 17
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                z: 4
                font.pixelSize: 12
            }
        }

        Rectangle {
            id: rectangle6
            x: 364
            y: 101
            width: 80
            height: 80
            color: "#000000"
            radius: 50
            opacity: 1
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.right: parent.right
            anchors.rightMargin: 5
            rotation: 270
            Text {
                id: element11
                rotation: 90
                color: "#ffffff"
                text: batteri.getcharge
                anchors.verticalCenterOffset: 0
                anchors.horizontalCenterOffset: 0
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                z: 14
                font.pixelSize: 25

            }
            Text {
                id: element2
                x: 25
                y: 51
                width: 28
                color: "#323232"
                text: qsTr("%")
                anchors.verticalCenterOffset: 27
                anchors.right: parent.right
                anchors.rightMargin: 27
                anchors.verticalCenter: parent.verticalCenter
                rotation: 90
                z: 8
                font.pixelSize: 12
            }

            Rectangle {
                id: rectangle0
                x: 27
                y: 6
                width: 29
                height: 16
                color: "#323232"

                Rectangle {
                    id: rectangle10
                    x: 26
                    y: 3
                    width: 6
                    height: 10
                    color: "#323232"
                    radius: 1
                }
            }

            Rectangle {
                id: rectangle7
                x: 47
                y: -415
                width: 40
                height: 40
                color: "#000000"
                rotation: 90
                anchors.right: parent.right
                anchors.rightMargin: 40
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0

                Rectangle {
                    id: rectangle13
                    x: 5
                    y: 34
                    width: 2
                    height: 10
                    color: "#323232"
                    anchors.right: parent.right
                    anchors.rightMargin: 5
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 5
                }

                Rectangle {
                    id: rectangle14
                    x: 9
                    y: 38
                    width: 2
                    height: 10
                    color: "#323232"
                    anchors.right: parent.right
                    anchors.rightMargin: 10
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    rotation: 90
                }
            }

        }

        CircleSlider {
            x: 0
            y: 0
            width: 50
            height: 50
            anchors.rightMargin: 193
            anchors.bottomMargin: -85
            anchors.leftMargin: -193
            anchors.topMargin: 85
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            strokeColor: "#323232"
            rotation: 90
            _radius: 30
            max: 50
            scale: 1.06
            z: 9
            min: 0
            lineWidth: 4
        }

        CircleSlider {
            id: circleSlider1
            x: 0
            y: 0
            width: 50
            height: 51
            anchors.rightMargin: 188
            anchors.bottomMargin: -41
            anchors.bottom: parent.bottom
            anchors.top: parent.top
            anchors.leftMargin: -198
            anchors.topMargin: 129
            anchors.left: parent.left
            max:  acceleration.getspeed/1.2;
            state: ""
            _radius: 30
            strokeColor: "#2dcef9"
            lineWidth: 4
            transformOrigin: Item.Center
            rotation: -90
            z: 13
            scale: -1.06
            min: 0
        }

        CircleSlider {
            x: -4
            y: 149
            width: 60
            height: 60
            anchors.leftMargin: 99
            anchors.topMargin: 42
            anchors.left: parent.left
            anchors.rightMargin: -289
            anchors.bottomMargin: -128
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            rotation: -90
            _radius: 30
            max: 49
            strokeColor: "#88f458"
            scale: 1.06
            z: 9
            min: 0
            lineWidth: 4
        }

        CircleSlider {
            id: circleSlider2
            x: -4
            y: -4
            width: 60
            height: 60
            anchors.rightMargin: -6
            state: ""
            anchors.topMargin: 163
            max: -batteri.getcharge/2.05
            anchors.leftMargin: 382
            _radius: 30
            strokeColor: "#323232"
            lineWidth: 5
            anchors.bottomMargin: -7
            transformOrigin: Item.Center
            rotation: -90
            z: 13
            scale: -1.06
            min: 50


        }
    }


