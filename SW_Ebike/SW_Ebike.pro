TEMPLATE = app
QT += qml quick widgets quickwidgets multimedia location virtualkeyboard webengine
CONFIG += c++11

SOURCES += main.cpp \
    battery.cpp \
    clockcircle.cpp \
    distance.cpp \
    map.cpp \
    music.cpp \
    speedometer.cpp \
    turn_signal_light.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH = $$PWD/designer

# Default rules for deployment.
include(deployment.pri)
DISTFILES +=
HEADERS += \
    battery.h \
    clockcircle.h \
    def_types.h \
    distance.h \
    map.h \
    music.h \
    speedometer.h \
    turn_signal_light.h

# Add wiringPi library when linux flag is true.
unix:!macx: LIBS += -L$$PWD/libWiringPi/ -lwiringPi

INCLUDEPATH += $$PWD/libWiringPi
DEPENDPATH += $$PWD/libWiringPi

unix:!macx: LIBS += -L$$PWD/libWiringPi/ -lwiringPiDev

INCLUDEPATH += $$PWD/libWiringPi
DEPENDPATH += $$PWD/libWiringPi




