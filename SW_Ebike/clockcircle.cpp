#include "clockcircle.h"

ClockCircle::ClockCircle(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_backgroundColor(Qt::white),
    m_borderActiveColor(Qt::blue),
    m_borderNonActiveColor(Qt::gray),
    m_angle(0),
    m_circleTime(QTime(0,0,0,0))
{
    internalTimer = new QTimer(this);

    connect(internalTimer, &QTimer::timeout, [=](){
        setAngle(angle() - 0.3);
        setCircleTime(circleTime().addMSecs(50));
        update();
    });
}

ClockCircle::~ClockCircle()
{
    delete internalTimer;
}

/** This method changes the color of the cercle
 *
 * @param[out,global private]  m_backgroundColor
 * @param[out,global private]  m_borderActiveColor
 * @param[out,global private]  m_backgroundColor
 * @param[out,global private]  m_borderNonActiveColor
 * @return  void
 */
void ClockCircle::paint(QPainter *painter)
{
    QBrush  brush(m_backgroundColor);
    QBrush  brushActive(m_borderActiveColor);
    QBrush  brushNonActive(m_borderNonActiveColor);
    painter->setPen(Qt::NoPen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->setBrush(brushNonActive);
    painter->drawEllipse(boundingRect().adjusted(1,1,-1,-1));
    painter->setBrush(brushActive);
    painter->drawPie(boundingRect().adjusted(1,1,-1,-1),
                     90*16,
                     m_angle*16);
}

/** This method return to zero
 *
 * @param[in,out,global private]  QTime
 * @return  void
 */
void ClockCircle::clear()
{
    setCircleTime(QTime(0,0,0,0));
    setAngle(0);
    update();
    emit cleared();
}

/** This method starts the chronometer
 *
 * @param[out,global private]  internalTimer
 * @return  void
 */
void ClockCircle::start()
{
    internalTimer->start(50);
}


/** This method stops the chronometer
 *
 * @param[out,global private]  internalTimer
 * @return  void
 */
void ClockCircle::stop()
{
    internalTimer->stop();
}

/** This method returns name
 *
 * @param[out,global private]  m_name
 * @return  qstring
 */

QString ClockCircle::name() const
{
    return m_name;
}


/** This method returns background color
 *
 * @param[out,global private]  m_backgroundColor
 * @return  qcolor
 */
QColor ClockCircle::backgroundColor() const
{
    return m_backgroundColor;
}


/** This method returns active border's color
 *
 * @param[out,global private]  m_borderActiveColor
 * @return  qcolor
 */
QColor ClockCircle::borderActiveColor() const
{
    return m_borderActiveColor;
}


/** This method returns non active border's color
 *
 * @param[out,global private]  m_borderNonActiveColor
 * @return  qcolor
 */
QColor ClockCircle::borderNonActiveColor() const
{
    return m_borderNonActiveColor;
}


/** This method returns angle
 *
 * @param[out,global private]  m_angle
 * @return  qreal
 */
qreal ClockCircle::angle() const
{
    return m_angle;
}


/** This method returns circle time
 *
 * @param[out,global private]  m_circleTime
 * @return  qreal
 */
QTime ClockCircle::circleTime() const
{
    return m_circleTime;
}


/** This method defines name
 *
 * @param[in,out,global private]  m_name
 * @param[in,global private]      name
 * @return  void
 */

void ClockCircle::setName(const QString name)
{
    if (m_name == name)
        return;

    m_name = name;
    emit nameChanged(name);
}


/** This method defines background color
 *
 * @param[in,out,global private]  m_backgroundColor
 * @param[in,global private]      backgroundColor
 * @return  void
 */
void ClockCircle::setBackgroundColor(const QColor backgroundColor)
{
    if (m_backgroundColor == backgroundColor)
        return;

    m_backgroundColor = backgroundColor;
    emit backgroundColorChanged(backgroundColor);
}


/** This method defines the color of active border
 *
 * @param[in,out,global private]  m_borderActiveColor
 * @param[in,global private]      borderActiveColor
 * @return  void
 */
void ClockCircle::setBorderActiveColor(const QColor borderActiveColor)
{
    if (m_borderActiveColor == borderActiveColor)
        return;

    m_borderActiveColor = borderActiveColor;
    emit borderActiveColorChanged(borderActiveColor);
}


/** This method defines the color of non active border
 *
 * @param[in,out,global private]  m_borderNonActiveColor
 * @param[in,global private]      borderNonActiveColor
 * @return  void
 */
void ClockCircle::setBorderNonActiveColor(const QColor borderNonActiveColor)
{
    if (m_borderNonActiveColor == borderNonActiveColor)
        return;

    m_borderNonActiveColor = borderNonActiveColor;
    emit borderNonActiveColorChanged(borderNonActiveColor);
}


/** This method defines the angles
 *
 * @param[in,out,global private]  m_angle       {values: -360 .. 0}
 * @param[in,global private]      angle
 * @return  void
 */
void ClockCircle::setAngle(const qreal angle)
{
    if (m_angle == angle)
        return;

    m_angle = angle;


    if(m_angle <= -360) m_angle = 0;
    emit angleChanged(m_angle);
}



/** This method defines the circle time
 *
 * @param[in,out,global private]  m_circleTime
 * @param[in,global private]      circleTime
 * @return  void
 */
void ClockCircle::setCircleTime(const QTime circleTime)
{
    if (m_circleTime == circleTime)
        return;

    m_circleTime = circleTime;
    emit circleTimeChanged(circleTime);
}
