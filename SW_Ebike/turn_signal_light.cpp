#include "turn_signal_light.h"
#ifdef LINUX_VERSION
#include <wiringPi.h>
#endif

turn_signal_light::turn_signal_light(QObject *parent) : QObject(parent),m_lightRight(0),m_timeRight(0),m_timeLeft(0), m_lightLeft(0) {
    this->t_initGpioLeftAndRight();
    timer = new QTimer(this);
    timer->setInterval(T_TIME_INTERVAL_MS);
    connect(timer,&QTimer::timeout,this,&turn_signal_light::t_turnRight);
    connect(timer,&QTimer::timeout,this,&turn_signal_light::t_turnLeft);
    timer->start(T_TIME_START_MS);
}

turn_signal_light::~turn_signal_light() {
    delete timer;
}

#ifdef LINUX_VERSION
void turn_signal_light::t_initGpioLeftAndRight() {
   wiringPiSetup() ;
   pinMode(0, OUTPUT);
   pinMode(2, OUTPUT);

   // Flash twice when the system is under power ON
   for(quint32 i=0; i<2 ; i++) {
       digitalWrite(0, HIGH);
       digitalWrite(2, HIGH);
       delay(1000);
       digitalWrite(0, LOW);
       digitalWrite(2, LOW);
       delay(1000);
   }
}
#endif

/** This function display  the right turn signal
 *
 * @param[in,out,global private]  m_timeRight      time in second             {values: 259 .. 280}
 * @param[in,out,global private]  m_lightRight     right turn signal size     {values: 0 .. 10}
 * @return  void
 */
void turn_signal_light::t_turnRight() {
#ifdef MODE_TEST_ACTIVE

    if(m_timeRight < 283) {

        if (m_timeRight > 261) {

            m_lightRight += 10;
#ifdef LINUX_VERSION
             digitalWrite(2, HIGH);
#endif
             if (m_lightRight > 10) {

                 m_lightRight -= 20;
#ifdef LINUX_VERSION
                 digitalWrite(2, LOW);
#endif

             } else {

                 m_timeRight += 1;

             }

        emit t_valueChanged();

#if MODE_TEST_DEBUG_ACTIVE == 2
        qDebug() << t_lightRight;
#endif
        }

    m_timeRight += 1;

    }
#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else

#endif
}

/** This function display turn right signal
 *
 * @param[out,global private]     m_lightRight    right turn signal size     {values: 0 .. 10}
 * @return  int
 */
qint32 turn_signal_light::t_getRight()
 {
    return m_lightRight;
 }

/** This function display  the left turn signal
 *
 * @param[in,out,global private]     m_timeLeft      time in second          {values: 259 .. 280}
 * @param[in,out,global private]     m_lightLeft     left  turn signal size  {values: 0 .. 10}
 * @return  void
 */
void turn_signal_light::t_turnLeft() {
#ifdef MODE_TEST_ACTIVE
    if(m_timeLeft < 312) {

        if (m_timeLeft > 287) {

             m_lightLeft += 10;
#ifdef LINUX_VERSION
             digitalWrite(0, HIGH);
#endif
             if (m_lightLeft > 10) {

                 m_lightLeft -= 20;
#ifdef LINUX_VERSION
                 digitalWrite(0, LOW);
#endif
             } else {

                 m_timeLeft += 1;
             }

        emit t_valueChanged();

        }
        m_timeLeft += 1;
    }
#elif MODE_NORMAL_ACTIVE

/*Mode normal */

#else

#endif
}

/** This function display turn right signal
 *
 * @param[out,global private]     m_lightLeft    left turn signal size     {values: 0 .. 10}
 * @return  int
 */
qint32 turn_signal_light::t_getLeft()
 {
    return m_lightLeft;
 }
