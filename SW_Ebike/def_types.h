#ifndef DEF_TYPES_H
#define DEF_TYPES_H

#define SWITCH_ON  0U
#define SWITCH_OFF 1U

#if defined (SWITCH_OFF) && SWITCH_OFF == 1U
#define MODE_TEST_ACTIVE         1U
#define MODE_TEST_DEBUG_ACTIVE   1U
#elif defined (SWITCH_ON) && SWITCH_ON == 1U
#define MODE_NORMAL_ACTIVE       1U
#define MODE_TEST_DEBUG_ACTIVE   0U
#endif


#ifndef WIN32
#define LINUX_VERSION    (1U)    //Define new enviromment variable for LINUX
#endif

#endif // DEF_TYPES_H
