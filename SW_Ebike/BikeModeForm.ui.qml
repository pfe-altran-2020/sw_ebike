import QtQuick 2.0
import QtQuick.Controls.Material 2.0
import QtQuick.Controls 2.0

Item {
    id: element

    width: 480
    height: 260

    Rectangle {
        id: rectangle
        color: "#323232"
        anchors.fill: parent
    }
}
