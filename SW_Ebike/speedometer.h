#ifndef SPEEDOMETER_H
#define SPEEDOMETER_H
#include <QObject>
#include <QTimer>
#include <QDebug>

class speedometer : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(qreal getspeed READ s_getSpeed NOTIFY s_valueChanged)
    explicit speedometer(QObject *parent = nullptr);
    virtual ~speedometer();
    qreal s_getSpeed();

public slots :
     void s_increaseSpeed(void);

signals :
     void s_valueChanged();

private :
    qreal m_speed;
    qreal m_timer;
    QTimer *timer;
};

#endif // speedometer_H
