#include "map.h"
#include <QDebug>

Map::Map() : m_coordinate(0)
{

}

Map::~Map()
{
    delete this;
}

/** This function returns GPS Coordinates
 *
 * @param[out,global private]  GPS Coordinates
 * @return  qint32
 */
qint32 Map::coordinate()
{
    return m_coordinate;
}

/** This function set and transfer the GPS coordinates at graphic layer
 * @param[in, function parameter]    coordinate      GPS Coordinates  {values: 0U .. MAX_UINT32}
 * @param[in,out,global private]     m_coordinate    GPS Coordinates  {values: 0U .. MAX_UINT32}
 * @return  void
 */
void Map::setcoordinate(qint32 coordinate)
{
    if(m_coordinate != coordinate) {

        m_coordinate = coordinate;

        emit valueChanged(m_coordinate);

    } else {
        /* Do Nothing */
    }
}

/** This function set the GPS coordinates
 * @param[in, function parameter]     coordinate    GPS Coordinates  {values: 0U .. MAX_UINT32}
 * @return  void
 */
void Map::receivecoordinate(qint32 coordinate)
{
    setcoordinate(coordinate);
}
