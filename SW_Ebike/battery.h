#ifndef BATTERY_H
#define BATTERY_H

#include<QTimer>
#include<QDebug>

#define B_TIME_INTERVAL_MS  (100)
#define B_TIME_START_MS     (500)

class battery : public QObject
{
    Q_OBJECT
public:
    explicit battery(QObject *parent = nullptr);
    virtual ~battery();
    Q_PROPERTY(qint32 getcharge READ  b_getCharge NOTIFY b_valueChanged)
    Q_PROPERTY(qint32 getse1charged READ b_getSegOneCharged NOTIFY b_valueChanged)
    Q_PROPERTY(qint32 getse2charged READ b_getSegTwoCharged NOTIFY b_valueChanged)
    Q_PROPERTY(qint32 getse3charged READ b_getSegThreeCharged NOTIFY b_valueChanged)
    Q_PROPERTY(qint32 getse4charged READ b_getSegFourCharged NOTIFY b_valueChanged)
    Q_PROPERTY(qint32 getse5charged READ b_getSegFiveCharged NOTIFY b_valueChanged)
    Q_PROPERTY(qint32 getdecharge READ b_getBatteryDecharge NOTIFY b_valueChanged)





qreal b_getCharge();
qreal b_getSegOneCharged();
qreal b_getSegTwoCharged();
qreal b_getSegThreeCharged();
qreal b_getSegFourCharged();
qreal b_getSegFiveCharged();
qreal b_getBatteryDecharge();


public slots :
     void b_State_of_charge(void);
     void b_chargeSegOne(void);
     void b_chargeSegTwo(void);
     void b_chargeSegThree(void);
     void b_chargeSegFour(void);
     void b_chargeSegFive(void);
     void b_batteryDecharge(void);



signals :
     void b_valueChanged();

private :
    qreal  m_charge;
    qint32 m_segOneCounter;
    qint32 m_segOne;
    qint32 m_segTwoCounter;
    qint32 m_segTwo;
    qint32 m_segThreeCounter;
    qint32 m_segThree;
    qint32 m_segFourCounter;
    qint32 m_segFour;
    qint32 m_segFiveCounter;
    qint32 m_segFive;
    qreal  m_decharge;
    qint32 m_timeDecharge;
    qint32 m_time;
    QTimer *timer;
};



#endif // BATTERY_H





