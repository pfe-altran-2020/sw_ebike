import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.1
import Qt.labs.folderlistmodel 2.12
SpeedForm {



        QtObject {
            property var locale: Qt.locale()
            property date currentDate: new Date()
            property string dateString
            property string timeString

            Component.onCompleted: {
                dateString = currentDate.toLocaleDateString();
                timeString = currentDate.toLocaleTimeString();
            }
        }


        Speedometer {
            id: speedometer
            x: 0
            y: -131
            height: 525
            color: "#323232"
            border.color: "#323232"
            opacity: 1
            z: 3
            width: 513


            Rectangle {
                id: rectangle3
                x: 11
                y: 138
                width: 252
                height: 247
                radius:128.5
                gradient: Gradient {
                    GradientStop {
                        position: 0.006
                        color: "#ffffffff"
                    }

                    GradientStop {
                        position: 0.884
                        color: "#ff000000"
                    }

                    GradientStop {
                        position: 0.61
                        color: "#000000"
                    }
                }
                z: 0

                Text {
                    id: element8
                    x: -8
                    y: 5
                    color: "#ffffff"
                    text:   new Date().toLocaleTimeString(Qt.locale(),"h:mm") //ddd MMM d //h:mm AP
                    font.pixelSize: 20
                }
            }

            Rectangle {
                id: rectangle1
                x: 95
                y: 344
                width: 199
                height: 40
                color: "#2dcef9"
                radius: 5
                clip: true
                z: 2
            }

            Rectangle {
                id: rectangle
                x: 96
                y: 375
                width: 200
                height: 11
                color: "#323232"
            }

            Rectangle {
                id: rectangle5
                x: 42
                y: 298
                width: 100
                height: 40
                color: "#000000"
            }

            Rectangle {
                id: rectangle9
                x: 45
                y: 169
                width: 180
                height: 180
                color: "#313232"
                radius: 90
                clip: false
                z: 2

                Rectangle {
                    id: rectangle17
                    x: 194
                    y: 45
                    width: 3
                    height: 39
                    rotation: 90
                    gradient: Gradient {
                        GradientStop {
                            position: 0.965
                            color: "#323232"
                        }

                        GradientStop {
                            position: 0
                            color: "#000000"
                        }
                    }
                    z: 3
                }

                Rectangle {
                    id: rectangle43
                    x: -23
                    y: 31
                    width: 44
                    height: 3
                    color: "#313232"
                    rotation: 212
                }

                Rectangle {
                    id: rectangle4
                    x: 95
                    y: 64
                    width: 149
                    height: 116
                    color: "#313232"
                    z: 0

                    Rectangle {
                        id: rectangle11
                        x: 149
                        y: 37
                        width: 191
                        height: 114
                        color: "#cbcbcb"
                        z: 0

                        Rectangle {
                            id: rectangle13
                            x: 26
                            y: 60
                            width: 136
                            height: 53
                            color: "#000000"

                            Rectangle {
                                id: rectangle14
                                x: 118
                                y: 17
                                width: 30
                                height: 19
                                color: "#000000"
                                radius: 7
                            }



                            Rectangle {
                                id: seg3
                                x: 56
                                y: 0
                                width: partThree.getse3charged
                                height: 53
                                color: "#2dcef9"
                            }

                            Rectangle {
                                id: seg4
                                x: 84
                                y: 0
                                width: partFour.getse4charged
                                height: 53
                                color: "#2dcef9"
                            }

                            Rectangle {
                                id: seg5
                                x: 111
                                y: 0
                                width: partFive.getse5charged
                                height: 53
                                color: "#2dcef9"
                            }

                            Rectangle {
                                id: batdecharge
                                x: 136
                                y: 0
                                width: bat.getdecharge
                                height: 53
                                color: "#000000"
                                z: 7
                                anchors.right: parent.right
                                anchors.rightMargin: 0
                            }

                            Rectangle {
                                id: seg
                                x: 0
                                y: 0
                                width: partOne.getse1charged
                                height: 53
                                color: "#2dcef9"
                                z: 2
                            }

                            Rectangle {
                                id: segg
                                x: 28
                                y: 0
                                width: partTwo.getse2charged
                                height: 53
                                color: "#2dcef9"
                                z: 2
                            }
                        }

                        Text {
                            id: element
                            x: 40
                            y: 12
                            width: 84
                            height: 39
                            text: batteri.getcharge
                            font.bold: true
                            style: Text.Normal
                            textFormat: Text.RichText
                            wrapMode: Text.NoWrap
                            horizontalAlignment: Text.AlignHCenter
                            elide: Text.ElideNone
                            font.pixelSize: 40

                            Text {
                                id: element1
                                x: 85
                                y: 9
                                width: 32
                                height: 26
                                color: "#948989"
                                text: qsTr("%")
                                font.pixelSize: 28
                            }
                        }

                        Rectangle {
                            id: rectangle21
                            x: 11
                            y: 27
                            width: 16
                            height: 9
                            color: "#948989"

                            Rectangle {
                                id: rectangle42
                                x: 9
                                y: 3
                                width: 10
                                height: 3
                                color: "#948989"
                            }
                        }

                        Rectangle {
                            id: rectangle44
                            x: 6
                            y: 48
                            width: 28
                            height: 3
                            color: "#948989"
                        }

                        Rectangle {
                            id: rectangle12
                            x: 32
                            y: 43
                            width: 2
                            height: 6
                            color: "#948989"
                        }

                        Rectangle {
                            id: rectangle18
                            x: 6
                            y: 43
                            width: 2
                            height: 6
                            color: "#948989"
                        }

                        Rectangle {
                            id: rectangle22
                            x: 21
                            y: 46
                            width: 2
                            height: 4
                            color: "#948989"
                        }

                        Rectangle {
                            id: rectangle23
                            x: 16
                            y: 46
                            width: 2
                            height: 4
                            color: "#948989"
                        }

                        Rectangle {
                            id: rectangle26
                            x: 11
                            y: 46
                            width: 2
                            height: 4
                            color: "#948989"
                        }

                        Rectangle {
                            id: rectangle27
                            x: 26
                            y: 46
                            width: 2
                            height: 4
                            color: "#948989"
                        }

                    }

                    Rectangle {
                        id: rectangle46
                        x:107
                        y: 78
                        width: arrow.t_getright
                        height: arrow.t_getright
                        color: "#ffffff"
                        z: 0
                        rotation: 45
                    }

                    Rectangle {
                        id: rectangle15
                        x: 46
                        y: 78
                        width: arrow.t_getleft
                        height:arrow.t_getleft
                        color: "#ffffff"
                        rotation: 45
                    }
                }

                Rectangle {
                    id: rectangle28
                    x: 180
                    y: 142
                    width: arrow.t_getright+20
                    height: arrow.t_getright
                    color: "#ffffff"
                    z: 0
                }

                Rectangle {
                    id: rectangle8
                    x: 146
                    y: 142
                    width: arrow.t_getleft+20
                    height: arrow.t_getleft
                    color: "#ffffff"
                }
            }

            Rectangle {
                id: rectangle2
                x: 110
                y: 360
                width: 178
                height: 3
                color: "#000000"
                z: 2
            }

            Rectangle {
                id: rectangle6
                x: 110
                y: 370
                width: 178
                height: 3
                color: "#000000"
                z: 4
            }

            Rectangle {
                id: rectangle7
                x: 289
                y: 132
                width: 191
                height: 67
                color: "#cfcfcf"

                Rectangle {
                    id: rectangle10
                    x: 0
                    y: 68
                    width: 191
                    height: 78
                    color: "#4f4f4f"
                    z: 0

                    Text {
                        id: element2
                        x: 50
                        y: 12
                        width: 77
                        height: 45
                        color: "#ffffff"
                        text: batteri.getcharge*0.99
                        font.strikeout: false
                        font.italic: false
                        font.bold: true
                        font.pixelSize: 40
                    }

                    Text {
                        id: element4
                        x: 126
                        y: 27
                        color: "#948989"
                        text: qsTr("km")
                        z: 2
                        font.pixelSize: 24
                    }

                    Rectangle {
                        id: rectangle29
                        x: 8
                        y: 36
                        width: 18
                        height: 18
                        color: "#948989"
                        radius: 9
                        z: 0
                    }

                    Rectangle {
                        id: rectangle30
                        x: 10
                        y: 38
                        width: 14
                        height: 14
                        color: "#4f4f4f"
                        radius: 7
                    }

                    Rectangle {
                        id: rectangle16
                        x: 129
                        y: 0
                        width: 62
                        height: 70
                        color: "#4f4f4f"
                    }
                }

                Rectangle {
                    id: rectangle31
                    x: 2
                    y: 112
                    width: 30
                    height: 3
                    color: "#948989"
                    rotation: 135
                }

                Text {
                    id: element5
                    x: 46
                    y: 15
                    width: 45
                    height: 48
                    text: counter.d_getdistance


                    horizontalAlignment: Text.AlignRight


                    font.bold: true
                    font.pixelSize: 40
                }

                Text {
                    id: element6
                    x: 94
                    y: 34
                    width: 20
                    height: 28
                    color: "#000000"
                    text: qsTr(",0")
                    font.bold: true
                    font.pixelSize: 21
                }

                Text {
                    id: element7
                    x: 123
                    y: 31
                    color: "#948989"
                    text: qsTr("km")
                    font.pixelSize: 24
                }
            }

            Rectangle {
                id: rectangle24
                x: 326
                y: 312
                width: 2
                height: 6
                color: "#948989"
            }

            Rectangle {
                id: rectangle25
                x: 320
                y: 315
                width: 2
                height: 3
                color: "#948989"
            }

            Rectangle {
                id: rectangle32
                x: 302
                y: 213
                width: 16
                height: 13
                color: "#948989"
                rotation: 0
            }

            Rectangle {
                id: rectangle33
                x: 305
                y: 202
                width: 13
                height: 19
                color: "#4f4f4f"
                rotation: 117
            }

            Rectangle {
                id: rectangle34
                x: 305
                y: 215
                width: 13
                height: 19
                color: "#4f4f4f"
                rotation: 241
            }

            Rectangle {
                id: rectangle35
                x: 320
                y: 182
                width: 2
                height: 4
                color: "#948989"
            }

            Rectangle {
                id: rectangle36
                x: 315
                y: 182
                width: 2
                height: 4
                color: "#948989"
            }

            Rectangle {
                id: rectangle37
                x: 300
                y: 184
                width: 28
                height: 3
                color: "#948989"
            }

            Rectangle {
                id: rectangle38
                x: 326
                y: 180
                width: 2
                height: 6
                color: "#948989"
            }

            Rectangle {
                id: rectangle39
                x: 300
                y: 179
                width: 2
                height: 6
                color: "#948989"
            }

            Rectangle {
                id: rectangle40
                x: 306
                y: 182
                width: 2
                height: 4
                color: "#948989"
            }

            Rectangle {
                id: rectangle41
                x: 311
                y: 182
                width: 2
                height: 4
                color: "#948989"
            }
        }

    }



