import QtQuick 2.4

IntroForm {
    id: introForm
    Image {
        id: image
        x: 111
        y: 99
        width: 258
        height: 63
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        source: "pics/logoaltran.svg"
        fillMode: Image.PreserveAspectFit

        OpacityAnimator on opacity{
            from: 0;
            to: 1;
            duration: 5000
        }

    }
}
