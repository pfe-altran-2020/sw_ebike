#ifndef MUSIC_H
#define MUSIC_H
#include <QTimer>
#include <QDebug>
#include <QMediaPlayer>


class music : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(unsigned int m_getprogressBar READ m_getprogressBar NOTIFY m_valueChanged)
    explicit music(QObject *parent = nullptr);
    virtual ~music();
    qint32 m_getprogressBar();
    qint64 m_getprogressTime();

public slots :
     void m_play(void);
     void onClickedstartstop(void);

signals :
     void m_valueChanged();

private :
    qint32 m_progressBar;
    qint64 m_progressTime;
    QTimer *timer;
    QMediaPlayer *m_player;
    QMediaPlayer *m_player1;

};



#endif // music_H
