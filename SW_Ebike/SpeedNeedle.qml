import QtQuick 2.4
import QtGraphicalEffects 1.0

Canvas {
    id: canvas

    property int value : 0

    onValueChanged: {zeiger.rotation = Math.min(Math.max(-250, canvas.value*3.5 - 149), -30); canvas.currentValue = zeiger.rotation - 211} //130 minrotation, -30 maxrotation
    width: parent.width; height: parent.height
    scale: 1
    rotation: 0
    z: 2
    contextType: qsTr("")
    opacity: 1
    clip: false
    visible: true

    Rectangle {
        id: zeiger
        y: 196
        rotation: -149 //siehe minrotation
        width:1
        height: 51  // needle size
        transformOrigin: Item.Bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.verticalCenter

        smooth: true
        antialiasing: true
        color: "#d5d5bb"
        anchors.horizontalCenterOffset: 2
        anchors.bottomMargin:4
        onRotationChanged: {canvas.currentValue = zeiger.rotation - 211; canvas.requestPaint()}//texti.text = zeiger.rotation

            Behavior on rotation {
                NumberAnimation{
                    duration: 15000
                    easing.type: Easing.OutCirc
                }
            }
    }


      antialiasing: true

      property color secondaryColor: zeiger.color

      property real centerWidth: width / 2
      property real centerHeight: height / 2
      property real radius: Math.min(canvas.width, canvas.height) / 2.4  //taille du cercle rouge 2.4

      property real minimumValue: -360
      property real maximumValue: 0
      property real currentValue: -360

      // this is the angle that splits the circle in two arcs
      // first arc is drawn from 0 radians to angle radians
      // second arc is angle radians to 2*PI radians
      property real angle: (currentValue - minimumValue) / (maximumValue - minimumValue) * 4 * Math.PI + 0
      property real angleOffset: 20.955
      x: 0
      y: 0 //to start at 0mph //-Math.PI / 2


      onPaint: {
          var ctx = getContext("2d");
          ctx.save();

          var gradient2 = ctx.createRadialGradient((parent.width / 2),(parent.height / 2), 0, (parent.width / 2),(parent.height / 2),parent.height);
           gradient2.addColorStop(0.1, "#2DCEF9");   //oben
          gradient2.addColorStop(1, "#2DCEF9");   //oben
          gradient2.addColorStop(0.45, "#2DCEF9");   //mitte
          gradient2.addColorStop(0,5, "transparent");   //unten

          ctx.clearRect(0, 10, canvas.width, canvas.height);

          ctx.beginPath();
          ctx.lineWidth = 180;
          ctx.strokeStyle = gradient2
          ctx.arc(canvas.centerWidth, canvas.centerHeight, canvas.radius - (ctx.lineWidth /1.1), canvas.angleOffset, canvas.angleOffset + canvas.angle);
          ctx.stroke();

          ctx.restore();
      }
}
