import QtQuick 2.0
import QtGraphicalEffects 1.0



Canvas {

   id: root
   property real min: 0
   property real max: 100
   property color strokeColor: "#2dcef9"
   property int lineWidth: 10

   /*!
      \internal
   */


   property int _radius: (root.width - root.lineWidth) * .5
   property int _centerX: root.width * .5
   property int _centerY: root.height * .5
   width: 480
   height: 255

   anchors.fill: parent
   //anchors.margins: 20

   onMaxChanged: {
      requestPaint()
   }

   onMinChanged: {
      requestPaint()
   }

   onPaint: drawCircle()

   function drawCircle() {

      var ctx = root.getContext("2d");
      var startArcPt = root.min * .01 * (2*Math.PI)
      var endArcPt = root.max * .01 * (2*Math.PI)

      ctx.clearRect(0,0,root.width,   root.height)
      ctx.strokeStyle = root.strokeColor
      ctx.lineWidth = root.lineWidth
      ctx.beginPath()
      ctx.arc(root._centerX,root._centerY,root._radius,startArcPt,endArcPt)
      ctx.stroke()

   }

}
