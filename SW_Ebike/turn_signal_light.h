#ifndef TURN_SIGNAL_LIGHT_H
#define TURN_SIGNAL_LIGHT_H
#include <QObject>
#include <QTimer>
#include <QDebug>
#include <def_types.h>

#define T_TIME_INTERVAL_MS  (100)
#define T_TIME_START_MS     (400)



class turn_signal_light : public QObject
{
    Q_OBJECT
public:
    Q_PROPERTY(qint32 t_getright READ t_getRight NOTIFY t_valueChanged)
    Q_PROPERTY(qint32 t_getleft READ t_getLeft NOTIFY t_valueChanged)
    explicit turn_signal_light(QObject *parent = nullptr);
    virtual ~turn_signal_light();
    qint32 t_getRight();
    qint32 t_getLeft();
#ifdef LINUX_VERSION
    void t_initGpioLeftAndRight();
#endif

    public slots :
         void t_turnRight(void);
         void t_turnLeft(void);

    signals :
         void t_valueChanged();

    private :
        qint32 m_lightRight;
        qint32 m_timeRight;
        qint32 m_timeLeft;
        qint32 m_lightLeft;
        QTimer *timer;

};

#endif // TURN_SIGNAL_LIGHT_H



